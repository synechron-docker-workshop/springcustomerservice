FROM openjdk
COPY target/customer-service-0.0.1-SNAPSHOT.jar /apps/
CMD ["java","-jar","/apps/customer-service-0.0.1-SNAPSHOT.jar"]

